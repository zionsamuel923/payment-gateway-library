# payment gateway library

Requirements
------------
 - Docker

Pre Requirements
------------
 - copy and update `.env` to the root of the repo according to `.env.example`

## Build and Start Docker Container
```
1. docker-compose build
2. docker-compose up -d
```

## Create Order Form
- BRAINTREE_TOKENIZATION_KEY -> can get from env file
```
http://localhost:8080/index.html?authorization=BRAINTREE_TOKENIZATION_KEY
```

## Check Order Payment Info
```
http://localhost:8080/paymentCheckingForm.html
```

## Data Strucutre

* ./app.ts -> app entry point, start the server and port
* ./.env -> all the environment variables
* ./app/Server.ts -> init server and config
    * ./app/controllers -> handle code logic
    * ./app/libs -> for export library instance braintree/redis/sequelize
    * ./app/models -> data model
    * ./app/repos/ -> repository for db query logic
    * 
* ./config/default.ts -> for app setting 
* ./config/Routes.ts -> for app route setting 
* ./public -> static file such as html, css, js file