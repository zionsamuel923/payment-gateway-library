import {Order, OrderInterface} from "../../models/Order";
import {CreateOptions, FindOptions} from "sequelize/types/lib/model";

export = {
    findAll(options: FindOptions) {
           return Order.findAll<Order>(options);
       },
    findByMatchedPreference(options: FindOptions) {
           return Order.findOne<Order>(options);
       },
    create(options: OrderInterface) {
           return Order.create<Order>(options);
    }
}