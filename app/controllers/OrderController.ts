import { Request, Response } from "express";
import { OrderInterface } from "../models/Order";
import { gateway } from "../libs/braintree";
import { redisClient } from "../libs/redis";
import orderRepo from "../repos/sequelize/order";

export class OrderController {

    public async index(req: Request, res: Response) {
        try {
            let orders = await orderRepo.findAll({});
            res.json({
                data: orders
            });
        } catch (error) {
            console.log(error.message, error.stack);
            console.error('Unable to connect to the database:', error);
            res.status(201).json(error);
        }
    }

    public async show(req: Request, res: Response) {
        if (!req.query || !req.query.customer_name || !req.query.payment_reference_code) {
            res.status(404).json({ errors: "Missing Params" });
        }

        let customerName: string = req.query.customer_name.toString();
        let transactionId: string  = req.query.payment_reference_code.toString();

        redisClient.exists(transactionId, async function(err, reply) {
            if (err) throw err;
            if (reply) {
                redisClient.get(transactionId, function(err, value: string) {
                    let order = JSON.parse(value);
                    if (customerName === order.customerName) {
                        res.json({
                            data: order
                        });
                    } else {
                        res.status(404).json({ errors: "Record not found" });
                    }
                });
            } else {
                let order = await orderRepo.findByMatchedPreference({
                    where: {
                        customerName: customerName,
                        transactionId: transactionId,
                    }
                });
                if (order) {
                    redisClient.set(order.transactionId!, JSON.stringify(order));
                    res.json({
                        data: order
                    });
                } else {
                    res.status(404).json({ errors: "Record not found" });
                }
            }
        });

    }

    public async create(req: Request, res: Response) {
        const params: OrderInterface = req.body;
        if (params.currency !== 'HKD') {
            res.status(400).json({ errors: "Coming soon", meta: ["currency"] });
        }
        try {
            const newOrder = await orderRepo.create(params);

            gateway.transaction.sale({
                amount: newOrder.price.toString(),
                orderId: newOrder.id.toString(),
                paymentMethodNonce: params.paymentMethodNonce,
                customer: {
                    firstName: newOrder.customerName,
                    phone: newOrder.customerPhoneNumber,
                },
                options: {
                    submitForSettlement: true
                }
            }).then(function (result) {
                if (result.success) {
                    newOrder.transactionId = result.transaction.id;
                    newOrder.transactionResponse = JSON.stringify(result);
                    newOrder.save();
                    redisClient.set(newOrder.transactionId, JSON.stringify(newOrder));
                    res.json({
                        data: newOrder
                    });
                } else {
                    console.error(result.message);
                    res.status(404).json({ errors: ["Record not found"] });
                }
            }).catch(function (err) {
                console.error(err);
            });

        } catch (error) {
            console.log(error.message, error.stack);
            console.error('Unable to connect to the database:', error);
            res.status(201).json(error);
        }
    }

}