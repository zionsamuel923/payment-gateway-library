import { Sequelize, Model, DataTypes, BuildOptions } from 'sequelize';
import { sequelize } from "../libs/sequelize";

export class Order extends Model {
    public id!: number;
    public customerName!: string;
    public customerPhoneNumber!: string;
    public currency!: string;
    public price!: number;
    public paymentGateway: string = 'Braintree';
    public transactionId!: string | null;
    public transactionResponse!: string | null;

    // timestamps!
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}

export interface OrderInterface {
    paymentMethodNonce: string;
    customerName: string;
    customerPhoneNumber: string;
    currency: string;
    price: number;
}

Order.init(
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        customerName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        customerPhoneNumber: {
            type: DataTypes.STRING,
            allowNull: false
        },
        currency: {
            type: DataTypes.ENUM('HKD', 'USD', 'AUD', 'EUR', 'JPY', 'CNY'),
            allowNull: false
        },
        price: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false
        },
        paymentGateway: {
            type: DataTypes.STRING,
            allowNull: true
        },
        transactionId: {
            type: DataTypes.STRING,
            allowNull: true
        },
        transactionResponse: {
            type: DataTypes.TEXT,
            allowNull: true
        },
    },
    {
        tableName: "orders",
        sequelize: sequelize // this bit is important
    }
  );
  
  Order.sync({ force: true }).then(() => console.log("Orders table created"));
  