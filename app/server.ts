import express from 'express';
import bodyParser from 'body-parser';
import { Routes } from "../config/Routes";

export class Server {
    public app: express.Application;
    public routePrv: Routes = new Routes();

    constructor() {
        // Create a new express app instance
        this.app = express();
        this.config();
        this.routePrv.routes(this.app);
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(express.static('/usr/src/app/public'));
    }
}