import redis from "redis";
import config from 'config';

const port: number = config.get('redis.port');

export const redisClient = redis.createClient({
    host: 'redis',
    port: port
});

redisClient.on('connect', function() {
    console.log('connected');
});

redisClient.on("error", function(error) {
    console.error(error);
});