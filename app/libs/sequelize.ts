import { Sequelize } from 'sequelize';

export const sequelize = new Sequelize('payment_gateway_library_db', 'username', 'password', {
    // sqlite! now!
    dialect: 'sqlite',
  
    // the storage engine for sqlite
    // - default ':memory:'
    storage: './payment_gateway_library_db.sqlite'
});