import braintree from 'braintree';
import config from 'config';

const merchantId: string = config.get('braintree.merchantId');
const publicKey: string = config.get('braintree.publicKey');
const privateKey: string = config.get('braintree.privateKey');

export const gateway = braintree.connect({
    environment: braintree.Environment.Sandbox,
    merchantId: merchantId,
    publicKey: publicKey,
    privateKey: privateKey
});

