export = {
    server: {
        environment: process.env.APP_ENV,
        baseUrl: process.env.APP_BASE_URL,
        port: process.env.APP_PORT || 8080,
    },
    database: {
        dialect: process.env.DB_CONNECTION,
        storage: process.env.DB_STORAGE,
        databaseName: process.env.DB_DATABASE,
    },
    braintree: {
        // either 'Sandbox' or 'Production'
        environment: process.env.BRAINTREE_ENV,
        merchantId: process.env.BRAINTREE_MERCHANT_ID,
        publicKey: process.env.BRAINTREE_PUBLIC_KEY,
        privateKey: process.env.BRAINTREE_PRIVATE_KEY,
    },
    redis: {
        hostName: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
    }
};
