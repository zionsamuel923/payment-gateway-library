import { Application, Router, Request, Response } from "express";
import { OrderController } from "../app/controllers/OrderController";

export class Routes {
    public orderController: OrderController = new OrderController();

    public routes(app: Application): void {
          app.route("/api/orders")
              .get(this.orderController.show)
              .post(this.orderController.create);

        app.route("/api/orders/all").get(this.orderController.index);
    }
}