import { Server } from './app/Server';
import config from 'config';

const port: number = config.get('server.port');

const server = new Server();

server.app.listen(port, () => console.log(`Server is listening on port ${port}`));